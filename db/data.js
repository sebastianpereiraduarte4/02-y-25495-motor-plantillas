const integrantes = [
    {nombre:"Sebastian",apellido:"Pereira", imagen: "/assets/Sebastian-foto.webp",descripcion:"Soy alumno de la Universidad Catolica, sede gurambaré y tranajo en cervepar", matricula: "Y25495"},
    {nombre:"Juan",apellido:"Aquino", imagen: "url",descripcion:"", matricula: "Y19937"},
    {nombre:"Junior",apellido:"Cabral", imagen: "url",descripcion:"", matricula: "Y25387"},
    {nombre:"Elvio",apellido:"Aguero", imagen: "url",descripcion:"", matricula: "Y19099"},
    {nombre:"Luis",apellido:"Delgado", imagen: "url",descripcion:"", matricula: "UG0085"},
  
];

const tipoMedia = [
    {nombre: "Youtube"},
    {nombre: "Dibujo"},
    {nombre: "Imagen"},
];

exports.media = [
    {src: null, url: 'https://www.youtube.com/embed/FRn6xXXF-7s?si=yVJP3egE0MB4siuE' , matricula: 'Y25495', tipoMedia: 'Youtube', titulo:'Mi Video Favorito en YouTube', alt:'Trailer de attack of titan' },
    {url: null, src: '/assets/Sebastian-Dibujo.jpg', matricula: 'Y25495', tipoMedia: 'Dibujo', titulo:'Dibujo favorito', alt: 'Dibujo una fogata' },
    {url: null, src: '/assets/Sebastian-foto.webp', matricula: 'Y25495', tipoMedia: 'Imagen', titulo:'Imagen favorita', alt: 'Dark Souls' },

    {src: null, url: 'https://www.youtube.com/embed/_Yhyp-_hX2s' , matricula: 'Y19937', tipoMedia: 'Youtube', titulo:'Mi Video Favorito en YouTube', alt:'Canción de Eminem'},
    {url: null, src: '/assets/Juan-Dibujo.jpg', matricula: 'Y19937', tipoMedia: 'Dibujo', titulo:'Dibujo favorito', alt: 'canasta de basketball' },
    {url: null, src: '/assets/Juan-foto.jpeg', matricula: 'Y19937', tipoMedia: 'Imagen', titulo:'Imagen favorita', alt: 'aro de basketball' },

    { src: null, url: 'https://www.youtube.com/embed/Tsnyq-3k7Bg?si=-Bzir8axv4WRU4MS&amp;controls=0' , matricula: 'Y25387', tipoMedia: 'Youtube', titulo:'Mi Video Favorito en YouTube', alt:'Video de Quantum fracture' },
    { url: null, src: '/assets/Junior-Dibujo.png', matricula: 'Y25387', tipoMedia: 'Dibujo', titulo:'Dibujo favorito', alt: 'Dibujo de jupiter'},
    { url: null, src: '/assets/Junior - Foto.jpg', matricula: 'Y25387', tipoMedia: 'Imagen',  titulo:'Imagen favorita', alt: 'Atardecer'},

    {src: null, url: 'https://www.youtube.com/embed/NynNdkY2gx0?si=nzQjEF5y6hY6uSOp' , matricula: 'Y19099', tipoMedia: 'Youtube', titulo:'Mi Video Favorito en YouTube', alt:'Video de jugadas de futbol'  },
    {url: null, src: '/assets/Elvio-Dibujo.png', matricula: 'Y19099', tipoMedia: 'Dibujo', titulo:'Dibujo favorito', alt: 'Dibujo de un partido de futbol' },
    {url: null, src: '/assets/Elvio-Foto.jpg', matricula: 'Y19099', tipoMedia: 'Imagen', titulo:'Imagen favorita', alt: 'El bicho, Cristiano Ronaldo' },

    {src: null, url: 'https://www.youtube.com/embed/Hv5ET7azgEk?si=WxryTH3oIMGTTD-Z' , matricula: 'UG0085', tipoMedia: 'Youtube', titulo:'Mi Video Favorito en YouTube', alt:'Video de la vida secreta de la mente' },
    {url: null, src: '/assets/Luis Delgado-2.png', matricula: 'UG0085', tipoMedia: 'Dibujo', titulo:'Dibujo favorito', alt: 'Dibujo de una PC' },
    {url: null, src: '/assets/Luis Delgado.jpeg', matricula: 'UG0085', tipoMedia: 'Imagen', titulo:'Imagen favorita', alt: 'Foto personal en un cerro' },
]    
exports.integrantes= integrantes;