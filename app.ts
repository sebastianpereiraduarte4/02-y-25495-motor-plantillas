import express from 'express'

const db = require("./db/data");
const hbs = require("hbs");

const app = express();

app.use(express.static('public'));
app.set('view engine', 'hbs');
app.set('views', __dirname + "/views");

hbs.registerPartials(__dirname + "/views/partials");

//ruta localhost
app.get("/", (request, response)=> {
    response.render("index",{
        integrantes:db.integrantes,
        
    });
});

const matriculas = [...new Set(db.media.map((item:any) => item.matricula))];

app.get("/:matricula", (request, response, next) => {
    const matricula = request.params.matricula;
    if (matriculas.includes(matricula)) {
        const integranteFilter =db.integrantes.filter((integrantes:any) => integrantes.matricula === matricula);
        const mediaFilter =db.media.filter((media:any) => media.matricula === matricula);
        response.render('integrante', {
            integrante: integranteFilter,
            integrantes:db.integrantes,
            tipoMedia:db.tipoMedia,
            media: mediaFilter
        });
    } else {
        next();
    }
});

app.get("/paginas/curso.html", (request, response)=> {
    response.render("curso");
});

app.get("/paginas/word_cloud.html", (request, response)=> {
    response.render("word_cloud");
});

app.use((req, res) => {
    res.status(404).render('./partials/error_404');
});

app.listen(8080, () => {
         console.log("El servidor se está ejecutando en http://localhost:8080");
});
